﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Controller2D))]
public class Player : MonoBehaviour {

    Vector3 velocity;

    public float jumpHeight = 4;
    public float timeToJumpApex = 0.5f;
    float moveSpeed = 6;
    float accelTimeOnAir = 0.5f;
    float accelTimeOnGround = 0.1f;

    float gravity;
    float jumpVelocity;
    float velocityXSmoothing;

    Controller2D controller;

	// Use this for initialization
	void Start () {
        controller = GetComponent<Controller2D>();
        gravity = -(2 * jumpHeight)/Mathf.Pow(timeToJumpApex, 2);
        jumpVelocity = Mathf.Abs(gravity * timeToJumpApex);
	}
	
	// Update is called once per frame
	void Update () {
        // reset velocity if collision occur
        if (controller.collisions.above || controller.collisions.below)
        {
            velocity.y = 0;
        }
        // jump
        if (Input.GetKeyDown(KeyCode.Space) && controller.collisions.below)
        {
            velocity.y = jumpVelocity;
        }
        //
        // get input
        Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        float targetVelocityX = input.x * moveSpeed;
        velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelTimeOnGround:accelTimeOnAir);
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
	}
}
