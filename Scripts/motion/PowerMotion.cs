﻿using UnityEngine;
using System.Collections;

public class PowerMotion : MonoBehaviour {

    MotionTargets targets;
    public bool constantSpeed;
    public float speed;

    [HideInInspector]
    public float t;
    float distance;

	// Use this for initialization
	void Start () {
        targets = GetComponent<MotionTargets>();
        t = 0.0f;
        distance = Vector3.Distance(targets.start.position, targets.end.position);
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log(t);
        t += Mathf.Pow(Time.deltaTime * speed, 3);
        Vector3 pos = Vector3.Lerp(targets.start.position, targets.end.position, t);
        transform.position = pos;
	}
}
