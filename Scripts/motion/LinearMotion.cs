﻿using UnityEngine;
using System.Collections;

public class LinearMotion : MonoBehaviour {

    MotionTargets targets;
    public bool constantSpeed;
    public float speed;

    [HideInInspector]
    public float t;
    float distance;

	// Use this for initialization
	void Start () {
        targets = GetComponent<MotionTargets>();
        t = 0.0f;
        distance = Vector3.Distance(targets.start.position, targets.end.position);
    }
	
	// Update is called once per frame
	void Update () {
        if (t < 1)
        {
            if (constantSpeed)
            {
                t += Time.deltaTime * (speed/distance);
            }
            else
            {
                t += Time.deltaTime * speed;
            }
            Vector3 pos = Vector3.Lerp(targets.start.position, targets.end.position, t);
            transform.position = pos;
        }
	}
}
